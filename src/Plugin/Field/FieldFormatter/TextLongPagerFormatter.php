<?php

namespace Drupal\text_long_pager\Plugin\Field\FieldFormatter;

use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'text_long_pager' formatter.
 *
 * @FieldFormatter(
 *   id = "text_long_pager",
 *   label = @Translation("Text Long Pager"),
 *   field_types = {
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   quickedit = {
 *     "editor" = "form"
 *   }
 * )
 */
class TextLongPagerFormatter extends FormatterBase {

  /**
   * HTML comment used as token to split Text long field into pages.
   */
  const PAGE_BREAK_HTML = '<!--text-long-pagebreak-->';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'first' => '« First',
      'previous' => '‹ Previous',
      'next' => 'Next ›',
      'last' => 'Last »',
      'quantity' => 9,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'first' => [
        '#title' => $this->t('First link text'),
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->getSetting('first'),
        '#required' => TRUE,
      ],
      'previous' => [
        '#title' => $this->t('Previous link text'),
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->getSetting('previous'),
        '#required' => TRUE,
      ],
      'next' => [
        '#title' => $this->t('Next link text'),
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->getSetting('next'),
        '#required' => TRUE,
      ],
      'last' => [
        '#title' => $this->t('Last link text'),
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->getSetting('last'),
        '#required' => TRUE,
      ],
      'quantity' => [
        '#title' => $this->t('Number of pages to show'),
        '#type' => 'number',
        '#size' => 10,
        '#default_value' => $this->getSetting('quantity'),
        '#min' => 1,
        '#required' => TRUE,
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('First link: @text', ['@text' => $this->getSetting('first')]);
    $summary[] = $this->t('Previous link: @text', ['@text' => $this->getSetting('previous')]);
    $summary[] = $this->t('Next link: @text', ['@text' => $this->getSetting('next')]);
    $summary[] = $this->t('Last link: @text', ['@text' => $this->getSetting('last')]);
    $summary[] = $this->t('Number of links: @quantity', ['@quantity' => $this->getSetting('quantity')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $output = [
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      ];
      $html = \Drupal::service('renderer')->render($output);
      $pages = explode(self::PAGE_BREAK_HTML, $html);
      if (count($pages) > 1) {
        $element = PagerSelectExtender::$maxElement++;
        pager_default_initialize(count($pages), 1, $element);
        $page = pager_find_page($element);
        $elements[$delta] = [
          'markup' => [
            '#markup' => $pages[$page],
          ],
          'pager' => [
            '#theme' => 'pager',
            '#element' => $element,
            '#parameters' => [],
            '#tags' => [
              $this->t($this->getSetting('first')),
              $this->t($this->getSetting('previous')),
              '',
              $this->t($this->getSetting('next')),
              $this->t($this->getSetting('last')),
            ],
            '#quantity' => $this->getSetting('quantity'),
            '#route_name' => '<none>',
          ],
        ];
      }
      else {
        $elements[$delta] = [
          '#markup' => $html,
        ];
      }
    }

    return $elements;
  }

}
